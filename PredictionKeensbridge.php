<?php if ( ! defined('ABS_PATH')) exit('Direct access not allowed!');

/*
Plugin Name: Keensbridge PredictionIO
Plugin URI: http://www.keensbridge.com/
Description: This plugin integrates OSclass with PredictionIO
Version: 0.1
Author: William Heng
Author URI: http://www.williamheng.com/
Short Name: prediction
Plugin update URI: none
Date: 12 Feb 2014
*/

// Load configuration
if (! file_exists(dirname(__FILE__).'/config.php')) {
	die("Configuration file is not found. Create config file from config-sample.php");
}
require_once dirname(__FILE__).'/config.php';

// Assume Prediction engine is hosted locally if not defined
if (! defined("PREDICT_HOST")) {
	define("PREDICT_HOST", "127.0.0.1");
}

if (! defined("PREDICT_PORT")) {
	define("PREDICT_PORT", "8000");
}

// Load PredictionIO PHP SDK
require_once dirname(__FILE__).'/vendor/autoload.php';

// Load KLogger
require_once dirname(__FILE__).'/KLogger/src/KLogger.php';

// Using PredictionIOClient namespace
use PredictionIO\PredictionIOClient;


// Main class extends Data Access Object
class PredictionKB extends DAO {
	private static $instance;
	private static $client;
	private static $logger;

	// Singleton class, return instance on use
	public static function getInstance() {
		if ( ! self::$instance instanceof self) {
			self::$instance = new self;
		}
		return self::$instance;
	}

	// Call parent constructor
	function __construct() {
		parent::__construct();

		// Initialise PredictionIOClient
		self::$client = PredictionIOClient::factory(array(
			"appkey" => APP_KEY,
			"apiurl" => PREDICT_HOST.':'.PREDICT_PORT
			));

		// Initialise KLogger
		self::$logger = KLogger::instance(LOG_FOLDER, KLogger::DEBUG);
	}

	// Return PredictionIOClient connection
	public function getClient() {
		return self::$client;
	}

	// Return KLogger instance
	public function getLogger() {
		return self::$logger;
	}

	// Get the item table's name
	public function getTableItem() {
		return DB_TABLE_PREFIX.'t_item';
	}

	// Get the user table's name
	public function getTableUser() {
		return DB_TABLE_PREFIX.'t_user';
	}

	// Get the item's title and description table
	public function getTableItemDescription() {
		return DB_TABLE_PREFIX.'t_item_description';
	}

	// Get the item info table's name
	public function getTableItemLocation() {
		return DB_TABLE_PREFIX.'t_item_location';
	}

	// Get the city table
	public function getTableCity() {
		return DB_TABLE_PREFIX.'t_city';
	}

	// Get the region table
	public function getTableRegion() {
		return DB_TABLE_PREFIX.'t_region';
	}

	// Get the car attribute table
	public function getTableCarAttr() {
		return DB_TABLE_PREFIX.'t_item_car_attr';
	}

	// Return all item entries in database
	public function getAllItems() {
		$this->dao->select();
		$this->dao->from($this->getTableItem());
		
		$result = $this->dao->get();
		if (!$result) {
			return array();
		}

		return $result->resultArray();
	}

	// Return all user entries in database
	public function getAllUsers() {
		$this->dao->select();
		$this->dao->from($this->getTableUser());

		$result = $this->dao->get();
		if (!$result) {
			return array();
		}

		return $result->resultArray();
	}

	// Get the title of the item
	public function getItemTitle($itemId) {
		$this->dao->select();
		$this->dao->from($this->getTableItemDescription());
		$this->dao->where('fk_i_item_id', $itemId);
		$result = $this->dao->get();

		if (!$result) {
			return '';
		}

		$result->row();
	}

	// Get the required info for an item
	public function getItemInfo($itemId) {
		// Set everything required to null
		$regionId = $cityId = $makeId = $modelId = $colorId = $year = $mileage = $engine_size = $transmission = NULL;
		
		// Get the related location info
		$this->dao->select();
		$this->dao->from($this->getTableItemLocation());
		$this->dao->where('fk_i_item_id', $itemId);
		$result = $this->dao->get();
		
		if ($result) {
			$item = $result->resultArray();
			foreach ($item as $i) {
				$regionId = $i['fk_i_region_id'];
				$cityId = $i['fk_i_city_id'];
			}
		}

		// Get the related car attributes info
		$this->dao->select();
		$this->dao->from($this->getTableCarAttr());
		$this->dao->where('fk_i_item_id', $itemId);
		$result = $this->dao->get();

		if ($result) {
			$attr = $result->resultArray();
			foreach ($attr as $i) {
				$makeId = $i['fk_i_make_id'];
				$modelId = $i['fk_i_model_id'];
				$colorId = $i['fk_i_color_id'];
				$year = $i['i_year'];
				$mileage = $i['i_mileage'];
				$engine_size = $i['i_engine_size'];
				$transmission = $i['e_transmission'];
			}
		}

		return array(
			'region' => $regionId,
			'city' => $cityId,
			'make' => $makeId,
			'model' => $modelId,
			'color' => $colorId,
			'year' => $year,
			'mileage' => $mileage,
			'engine_size' => $engine_size,
			'transmission' => $transmission
			);
	}

	// Register a user in PredictionIO
	public function predictionRegisterUser($id) {
		try {
			$command = self::$client->getCommand('create_user', array('pio_uid' => $id));
			$response = self::$client->execute($command);
		} catch (Exception $e) {
			// Fail to make connection with engine, log error
			self::$logger->logError('Unable to create user in PredictionIO', $e->getMessage());
		}
	}

	// Register an item in PredictionIO
	public function predictionRegisterItem($infoArray) {
		try {
			$command = self::$client->getCommand('create_item', $infoArray);
			$response = self::$client->execute($command);
		} catch (Exception $e) {
			// Fail to make connection with engine, log error
			self::$logger->logError('Unable to register item in PredictionIO.', $e->getMessage());
			self::$logger->logInfo('Item info: ', $item);
		}
	}

	// Register interaction between user and item
	public function predictionRegisterInteraction($userId, $itemId) {
		try {
			// Identify the user ID with the engine
			self::$client->identify($userId);
			
			// Interaction data
			$info = array(
				'pio_action' => 'view',
				'pio_iid' => $itemId
				);

			$command = self::$client->getCommand('record_action_on_item', $info);
			$response = self::$client->execute($command);
		} catch (Exception $e) {
			// Fail to connect to engine, log error
			self::$logger->logError('Unable to register interaction', $e->getMessage());
			self::$logger->logInfo('Array(userId, item) info: ', array($userId, $itemId));
		}
	}
}