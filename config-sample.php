<?php

/*
Plugin Name: Keensbridge PredictionIO
Plugin URI: http://www.keensbridge.com/
Description: This plugin integrates OSclass with PredictionIO
Version: 0.1
Author: William Heng
Author URI: http://www.williamheng.com/
Short Name: prediction
Plugin update URI: none
Date: 16 Feb 2014
*/

// Define important configuration

// PredictionIO configuration
// Host has to always start with http(s):// and NOT end with a trailing forward slash e.g. http://localhost
define("PREDICT_HOST", "http://localhost");
define("PREDICT_PORT", "8000");

// Define the total connection timeout and connecting timeout for PredictionIO
define('PREDICT_TIMEOUT', 10);
define('PREDICT_CONNECT_TIMEOUT', 1.5);

// PredictionIO app key
define("APP_KEY", "");

// KLogger log folder
define("LOG_FOLDER", dirname(__FILE__)."/log/");

// Set the timezone
// Timezones available: http://php.net/timezones
date_default_timezone_set("Asia/Kuala_Lumpur");

// Set the Prediction engine's name, one for recommendation and another for similarity
define("PREDICT_REC_ENGINE", "");
define("PREDICT_SIM_ENGINE", "");

?>