<?php

$listings = array();

foreach ($items as $itemId) {
	$search = new Search();
	$search->addItemId($itemId);
	$result = $search->doSearch();
	unset($search);

	$listings = array_merge($listings, $result);
}

View::newInstance()->_exportVariableToView('items', $listings);
View::newInstance()->_exportVariableToView('listType', 'items');
osc_current_web_theme_path('loop.php');

?>