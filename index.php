<?php if ( ! defined('ABS_PATH')) exit('Direct access not allowed!');

/*
Plugin Name: Keensbridge PredictionIO
Plugin URI: http://www.keensbridge.com/
Description: This plugin integrates OSclass with PredictionIO. Modified cars attributes plugin is needed.
Version: 0.1
Author: William Heng
Author URI: http://www.williamheng.com/
Short Name: prediction
Plugin update URI: none
Date: 11 Feb 2014
Dependencies: PredictionIO application, KLogger, Cars attributes plugin
*/

// Load main class
require_once 'PredictionKeensbridge.php';

function prediction_install() {
	// If database has already been populated, populate engine as well

	// Create users
	$users = PredictionKB::getInstance()->getAllUsers();

	foreach ($users as $user) {
		$userId = $user['pk_i_id'];
		PredictionKB::getInstance()->predictionRegisterUser($userId);
		PredictionKB::getInstance()->getLogger()->logInfo("Creating user ".$userId);
	}

	// Create items
	$items = PredictionKB::getInstance()->getAllItems();
	
	$itemInfo = PredictionKB::getInstance()->getItemInfo(2);

	foreach ($items as $item) {
		$itemId = $item['pk_i_id'];

		$itemInfo = PredictionKB::getInstance()->getItemInfo($itemId);

		PredictionKB::getInstance()->predictionRegisterItem(
			item_attr(
				$itemId,
				$item['fk_i_category_id'],
				$itemInfo['region'],
				$itemInfo['city'],
				$itemInfo['make'],
				$itemInfo['model'],
				$itemInfo['color'],
				$itemInfo['year'],
				$itemInfo['mileage'],
				$itemInfo['engine_size'],
				$itemInfo['transmission']
			));
		PredictionKB::getInstance()->getLogger()->logInfo("Creating item ".$itemId);
	}
}

// Register user in PredictionIO after user's registration
function post_user_register($userId) {
	PredictionKB::getInstance()->predictionRegisterUser($userId);
}

// Return a unified list of attributes to be added in the engine
function item_attr($id, $category, $region, $city, $make, $model, $color, $year, $mileage = 0, $engine_size = 0, $transmission) {
	return array(
		'pio_iid' => $id,
		'pio_itypes' => $category,
		'pio_region' => $region,
		'pio_city' => $city,
		'pio_make' => $make,
		'pio_model' => $model,
		'pio_color' => $color,
		'pio_year' => $year,
		'pio_mileage' => $mileage,
		'pio_engine_size' => $engine_size,
		'pio_transmission' => $transmission
		);
}

// Register item in PredictionIO after item is added
function post_item_register($item) {
	$category = (Params::getParam("fk_i_category_id") == '') ? 'other' : (int)Params::getParam("fk_i_category_id");

	$info = item_attr(
		$item['pk_i_id'],
		$category,
		$item['s_region'],
		$item['s_city'],
		Params::getParam("make"),
		Params::getParam("model"),
		Params::getParam("color"),
		Params::getParam("year"),
		Params::getParam("mileage"),
		Params::getParam("engine_size"),
		Params::getParam("transmission")
		);

	PredictionKB::getInstance()->predictionRegisterItem($info);
}

// Register user-to-item interaction with the engine if logged in
function pre_show_item_interaction($item) {
	if (osc_is_web_user_logged_in()) {
		$userId = osc_user_id();
		$itemId = $item['pk_i_id'];

		PredictionKB::getInstance()->predictionRegisterInteraction($userId, $itemId);
	}
}

function item_similar($item) {
	if ( ! osc_is_web_user_logged_in()) {
		$itemId = $item['pk_i_id'];

		$client = PredictionKB::getInstance()->getClient();
		$info = array(
			'pio_engine' => PREDICT_SIM_ENGINE,
			'pio_iid' => $itemId,
			'pio_n' => 6
			);

		// Initialise pio_items to inform theme whether recommendations are retrieved
		View::newInstance()->_exportVariableToView('pio_items', array());

		// Try to retrieve recommendations
		try {
			$command = $client->getCommand('itemsim_get_top_n', $info);
			$response = $client->execute($command);
			$items = @$response['pio_iids'];
			if ( ! $items) $items = array();

			// Refresh pio_items if items are really retrieved
			View::newInstance()->_exportVariableToView('pio_items', $items);

		} catch (Exception $e) {
			PredictionKB::getInstance()->getLogger()->logNotice("Unable to get similar items: " . $itemId);
		}
	}
}

// Get predictions for user
function item_recommend() {
	if (osc_is_web_user_logged_in()) {
		$client = PredictionKB::getInstance()->getClient();
		$userId = osc_user_id();
		
		$info = array(
			'pio_engine' => PREDICT_REC_ENGINE,
			'pio_n' => 6
			);

		// Initialise pio_items to inform theme whether recommendations are retrieved
		View::newInstance()->_exportVariableToView('pio_items', array());
		try {
			$client->identify($userId);
			$command = $client->getCommand('itemrec_get_top_n', $info);
			$response = $client->execute($command);
			$items = @$response['pio_iids'];
			if ( ! $items) $items = array();

			// Refresh pio_items if items are really retrieved
			View::newInstance()->_exportVariableToView('pio_items', $items);

		} catch (Exception $e) {
			PredictionKB::getInstance()->getLogger()->logNotice("Unable to get prediction: ". $userId.' '.$e->getMessage());
		}
	}
}

// Display the items
function display_items($item_array) {
	if (is_array($item_array) && count($item_array) > 0) {
		$items = $item_array;
		require_once 'item_recommended.php';
	}
}

// Needed to activate the plugin
osc_register_plugin(osc_plugin_path(__FILE__), 'prediction_install');

// After user is registered, register user in prediction
osc_add_hook('user_register_completed', 'post_user_register');

// After item is posted or edited, update item in prediction
// If item exists in engine, rewrite details
osc_add_hook('posted_item', 'post_item_register');
osc_add_hook('edited_item', 'post_item_register');

// View item, register interaction
osc_add_hook('show_item', 'pre_show_item_interaction');

// Retrieve prediction for visitors
osc_add_hook('pio_similar', 'item_similar');

// Retrieve prediction for users
osc_add_hook('pio_recommend', 'item_recommend');

// Display predictions after retrieving
osc_add_hook('pio_display', 'display_items');

?>