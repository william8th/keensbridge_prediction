<?php

require_once dirname(__FILE__).'/config.php';
require_once dirname(__FILE__).'/vendor/autoload.php';
require_once dirname(__FILE__).'/predis/autoload.php';
require_once dirname(__FILE__).'/KLogger/src/KLogger.php';

use PredictionIO\PredictionIOClient;
$client = PredictionIOClient::factory(array("appkey" => APP_KEY));

$redis = new Predis\Client();
$logger = new KLogger(LOG_FOLDER, KLogger::DEBUG);

try {
	$cmdSet = $redis->createCommand('KEYS');
	$cmdSet->setArguments(array('*'));
	$reply = $redis->executeCommand($cmdSet);

	foreach ($reply as $id) {
		$last_active = $redis->get($id);

		// Delete user after 10 minutes of inactivity = 600 seconds;
		if (time() - $last_active > REMOVE_INTERVAL) {
			try {
				$command = $client->getCommand('delete_user', array('pio_uid' => $id));
				$response = $client->execute($command);

				try {
					// Delete user ID from redis after removed from engine
					$cmdSet = $redis->createCommand('DEL');
					$cmdSet->setArguments(array($id));
					$redis->executeCommand($cmdSet);
				} catch (Exception $e) {
					// Unable to delete user in redis
					$logger->logError('Unable to delete user info from redis', $id);
				}
			} catch (Exception $e) {
				// Unable to delete user
				$logger->logError('Unable to delete user ID from engine', $id);
			}
		}
	}
} catch (Exception $e) {
	$logger->logError('Unable to perform cron job at '. time(), $e->getMessage());
}

?>